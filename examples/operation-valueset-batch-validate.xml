<?xml version="1.0" encoding="UTF-8"?>

<OperationDefinition xmlns="http://hl7.org/fhir">
  <id value="ValueSet-batch-validate"/>
  <text>
    <status value="generated"/>
    <div xmlns="http://www.w3.org/1999/xhtml">
      <h2>Batch Mode Validation</h2>
      <p>OPERATION: Batch Mode Validation</p>
      <div>
        <p>Validate a set of concepts against value sets in a single oeration</p>

        <p>Typically, this would be called when a validating a resource or document that contains many codes, to reduce network latency events</p>

      </div>
      <p>URL: [base]/ValueSet/$Batch Mode Validation</p>
      <p>Parameters</p>
      <table class="grid">
        <tr>
          <td>
            <b>Use</b>
          </td>
          <td>
            <b>Name</b>
          </td>
          <td>
            <b>Cardinality</b>
          </td>
          <td>
            <b>Type</b>
          </td>
          <td>
            <b>Binding</b>
          </td>
          <td>
            <b>Documentation</b>
          </td>
        </tr>
        <tr>
          <td>IN</td>
          <td>item</td>
          <td>0..*</td>
          <td></td>
          <td/>
          <td>
            <div>
              <p>An item to be validated</p>

            </div>
          </td>
        </tr>
        <tr>
          <td>IN</td>
          <td>item.concept</td>
          <td>1..1</td>
          <td>CodeableConcept</td>
          <td/>
          <td>
            <div>
              <p>A codeable concept to be validated. If the source is a coding, or a code/system pair, wrap it in a CodeableConcept. The server may validate that the codings are not in conflict with each other if more than one is present</p>

            </div>
          </td>
        </tr>
        <tr>
          <td>IN</td>
          <td>item.uri</td>
          <td>1..1</td>
          <td>uri</td>
          <td/>
          <td>
            <div>
              <p>The value set to validate the concept against. This is a logical value set identifier (i.e. ValueSet.identifier). The server must know the value set (e.g. it is defined explicitly in the server&#39;s value sets, or it is defined implicitly by some code system known to the server</p>

            </div>
          </td>
        </tr>
        <tr>
          <td>IN</td>
          <td>date</td>
          <td>0..1</td>
          <td>dateTime</td>
          <td/>
          <td>
            <div>
              <p>The date for which the validation should be checked. Normally, this is the current conditions (which is the default values) but under some circumstances, systems need to validate that a correct code was used at some point in the past. A typical example of this would be where code selection is constrained to the set of codes that were available when the patient was treated, not when the record is being edited. Note that which date is appropriate is a matter for implementation policy.</p>

            </div>
          </td>
        </tr>
        <tr>
          <td>OUT</td>
          <td>item</td>
          <td>0..*</td>
          <td></td>
          <td/>
          <td>
            <div>
              <p>The outcome of validating an item. There must be an entry for every item in the same order</p>

            </div>
          </td>
        </tr>
        <tr>
          <td>OUT</td>
          <td>item.result</td>
          <td>1..1</td>
          <td>boolean</td>
          <td/>
          <td>
            <div>
              <p>True if the concept details supplied are valid</p>

            </div>
          </td>
        </tr>
        <tr>
          <td>OUT</td>
          <td>item.message</td>
          <td>0..1</td>
          <td>string</td>
          <td/>
          <td>
            <div>
              <p>Error details, if result = false. If this is provided when result = true, the message carries hints and warnings</p>

            </div>
          </td>
        </tr>
        <tr>
          <td>OUT</td>
          <td>item.display</td>
          <td>0..1</td>
          <td>string</td>
          <td/>
          <td>
            <div>
              <p>A valid display for the concept if the system wishes to display this to a user</p>

            </div>
          </td>
        </tr>
      </table>
    </div>
  </text>
  <url value="http://hl7.org/fhir/OperationDefinition/ValueSet-batch-validate"/>
  <name value="Batch Mode Validation"/>
  <publisher value="HL7 (FHIR Project)"/>
  <contact>
    <telecom>
      <system value="url"/>
      <value value="http://hl7.org/fhir"/>
    </telecom>
    <telecom>
      <system value="email"/>
      <value value="fhir@lists.hl7.org"/>
    </telecom>
  </contact>
  <description value="Validate a set of concepts against value sets in a single oeration

Typically, this would be called when a validating a resource or document that contains many codes, to reduce network latency events"/>
  <status value="draft"/>
  <date value="2015-07-14T16:14:24+00:00"/>
  <kind value="operation"/>
  <code value="batch-validate"/>
  <system value="false"/>
  <type value="ValueSet"/>
  <instance value="false"/>
  <parameter>
    <name value="item"/>
    <use value="in"/>
    <min value="0"/>
    <max value="*"/>
    <documentation value="An item to be validated"/>
    <part>
      <name value="concept"/>
      <use value="in"/>
      <min value="1"/>
      <max value="1"/>
      <documentation value="A codeable concept to be validated. If the source is a coding, or a code/system pair, wrap it in a CodeableConcept. The server may validate that the codings are not in conflict with each other if more than one is present"/>
      <type value="CodeableConcept"/>
    </part>
    <part>
      <name value="uri"/>
      <use value="in"/>
      <min value="1"/>
      <max value="1"/>
      <documentation value="The value set to validate the concept against. This is a logical value set identifier (i.e. ValueSet.identifier). The server must know the value set (e.g. it is defined explicitly in the server&#39;s value sets, or it is defined implicitly by some code system known to the server"/>
      <type value="uri"/>
    </part>
  </parameter>
  <parameter>
    <name value="date"/>
    <use value="in"/>
    <min value="0"/>
    <max value="1"/>
    <documentation value="The date for which the validation should be checked. Normally, this is the current conditions (which is the default values) but under some circumstances, systems need to validate that a correct code was used at some point in the past. A typical example of this would be where code selection is constrained to the set of codes that were available when the patient was treated, not when the record is being edited. Note that which date is appropriate is a matter for implementation policy."/>
    <type value="dateTime"/>
  </parameter>
  <parameter>
    <name value="item"/>
    <use value="out"/>
    <min value="0"/>
    <max value="*"/>
    <documentation value="The outcome of validating an item. There must be an entry for every item in the same order"/>
    <part>
      <name value="result"/>
      <use value="out"/>
      <min value="1"/>
      <max value="1"/>
      <documentation value="True if the concept details supplied are valid"/>
      <type value="boolean"/>
    </part>
    <part>
      <name value="message"/>
      <use value="out"/>
      <min value="0"/>
      <max value="1"/>
      <documentation value="Error details, if result = false. If this is provided when result = true, the message carries hints and warnings"/>
      <type value="string"/>
    </part>
    <part>
      <name value="display"/>
      <use value="out"/>
      <min value="0"/>
      <max value="1"/>
      <documentation value="A valid display for the concept if the system wishes to display this to a user"/>
      <type value="string"/>
    </part>
  </parameter>
</OperationDefinition>