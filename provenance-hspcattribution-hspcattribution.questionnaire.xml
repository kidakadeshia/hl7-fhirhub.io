<?xml version="1.0" encoding="UTF-8"?>

<Questionnaire xmlns="http://hl7.org/fhir">
  <id value="qs1"/>
  <contained>
    <ValueSet>
      <id value="vs2"/>
      <url value="urn:uuid:a8f1b201-bc56-4e45-b348-caf146d9b466"/>
      <name value="Type options for Provenance.agent.actor"/>
      <description value="Type options for Provenance.agent.actor"/>
      <status value="active"/>
      <expansion>
        <identifier value="urn:uuid:617c527a-5052-4a8f-986e-5daa4306e837"/>
        <timestamp value="2015-07-14T16:19:16+00:00"/>
        <contains>
          <system value="http://hl7.org/fhir/resource-types"/>
          <code value="Practitioner"/>
          <display value="Practitioner"/>
        </contains>
        <contains>
          <system value="http://hl7.org/fhir/resource-types"/>
          <code value="RelatedPerson"/>
          <display value="RelatedPerson"/>
        </contains>
        <contains>
          <system value="http://hl7.org/fhir/resource-types"/>
          <code value="Patient"/>
          <display value="Patient"/>
        </contains>
        <contains>
          <system value="http://hl7.org/fhir/resource-types"/>
          <code value="Device"/>
          <display value="Device"/>
        </contains>
      </expansion>
    </ValueSet>
  </contained>
  <identifier>
    <system value="urn:ietf:rfc:3986"/>
  </identifier>
  <status value="draft"/>
  <date value="2015-02-07T00:00:00+00:00"/>
  <publisher value="U.S. Office of the National Coordinator (ONC)"/>
  <group>
    <extension url="http://hl7.org/fhir/Profile/questionnaire-extensions#flyover">
      <valueString value="Provenance of a resource is a record that describes entities and processes involved in producing and delivering or otherwise influencing that resource. Provenance provides a critical foundation for assessing authenticity, enabling trust, and allowing reproducibility. Provenance assertions are a form of contextual metadata and can themselves become important records with their own provenance. Provenance statement indicates clinical significance in terms of confidence in authenticity, reliability, and trustworthiness, integrity, and stage in lifecycle (e.g., Document Completion - has the artifact been legally authenticated), all of which may impact Security, Privacy, and Trust policies."/>
    </extension>
    <linkId value="Provenance"/>
    <title value="Who, What, When for a set of resources"/>
    <text value="Some parties may be duplicated between the target resource and its provenance.  For instance, the prescriber is usually (but not always) the author of the prescription resource. This resource is defined with close consideration for W3C Provenance."/>
    <required value="true"/>
    <repeats value="false"/>
    <group>
      <extension url="http://hl7.org/fhir/Profile/questionnaire-extensions#flyover">
        <valueString value="The logical id of the resource, as used in the url for the resource. Once assigned, this value never changes. The only time that a resource does not have an id is when it is being submitted to the server using a create operation. Bundles always have an id, though it is usually a generated UUID."/>
      </extension>
      <extension url="http://www.healthintersections.com.au/fhir/Profile/metadata#type">
        <valueString value="string"/>
      </extension>
      <linkId value="Provenance.id"/>
      <required value="false"/>
      <repeats value="true"/>
      <question>
        <linkId value="Provenance.id.value"/>
        <text value="Logical id of this artifact"/>
        <type value="string"/>
        <required value="false"/>
        <repeats value="false"/>
      </question>
    </group>
    <group>
      <extension url="http://hl7.org/fhir/Profile/questionnaire-extensions#flyover">
        <valueString value="The metadata about the resource. This is content that is maintained by the infrastructure. Changes to the content may not always be associated with version changes to the resource."/>
      </extension>
      <linkId value="Provenance.meta"/>
      <text value="Metadata about the resource"/>
      <required value="false"/>
      <repeats value="true"/>
    </group>
    <group>
      <extension url="http://hl7.org/fhir/Profile/questionnaire-extensions#flyover">
        <valueString value="A reference to a set of rules that were followed when the resource was constructed, and which must be understood when processing the content. Asserting this rule set restricts the content to be only understood by a limited set of trading partners. This inherently limits the usefulness of the data in the long term. However the existing health eco-system is highly fractured, and not yet ready to define, collect, and exchange data in a generally computable sense. Wherever possible, implementers and/or specification writers should avoid using this element as much as possible."/>
      </extension>
      <extension url="http://www.healthintersections.com.au/fhir/Profile/metadata#type">
        <valueString value="uri"/>
      </extension>
      <linkId value="Provenance.implicitRules"/>
      <required value="false"/>
      <repeats value="true"/>
      <question>
        <linkId value="Provenance.implicitRules.value"/>
        <text value="A set of rules under which this content was created"/>
        <type value="string"/>
        <required value="false"/>
        <repeats value="false"/>
      </question>
    </group>
    <group>
      <extension url="http://hl7.org/fhir/Profile/questionnaire-extensions#flyover">
        <valueString value="The base language in which the resource is written. Language is provided to support indexing and accessibility (typically, services such as text to speech use the language tag). The html language tag in the narrative applies  to the narrative. The language tag on the resource may be used to specify the language of other presentations generated from the data in the resource

Not all the content has to be in the base language. The Resource.language should not be assumed to apply to the narrative automatically. If a language is specified, it should it also be specified on the div element in the html (see rules in HTML5 for information about the relationship between xml:lang and the html lang attribute)."/>
      </extension>
      <extension url="http://www.healthintersections.com.au/fhir/Profile/metadata#type">
        <valueString value="code"/>
      </extension>
      <linkId value="Provenance.language"/>
      <required value="false"/>
      <repeats value="true"/>
      <question>
        <linkId value="Provenance.language.value"/>
        <text value="language"/>
        <type value="choice"/>
        <required value="false"/>
        <repeats value="false"/>
      </question>
    </group>
    <group>
      <extension url="http://hl7.org/fhir/Profile/questionnaire-extensions#flyover">
        <valueString value="A human-readable narrative that contains a summary of the resource, and may be used to represent the content of the resource to a human. The narrative need not encode all the structured data, but is required to contain sufficient detail to make it &quot;clinically safe&quot; for a human to just read the narrative. Resource definitions may define what content should be represented in the narrative to ensure clinical safety. Contained resources do not have narrative. Resources that are not contained SHOULD have a narrative."/>
      </extension>
      <linkId value="Provenance.text"/>
      <text value="Text summary of the resource, for human interpretation"/>
      <required value="false"/>
      <repeats value="true"/>
    </group>
    <group>
      <extension url="http://hl7.org/fhir/Profile/questionnaire-extensions#flyover">
        <valueString value="These resources do not have an independent existence apart from the resource that contains them - they cannot be identified independently, and nor can they have their own independent transaction scope. This should never be done when the content can be identified properly, as once identification is lost, it is extremely difficult (and context dependent) to restore it again."/>
      </extension>
      <linkId value="Provenance.contained"/>
      <text value="Contained, inline Resources"/>
      <required value="false"/>
      <repeats value="true"/>
    </group>
    <group>
      <extension url="http://hl7.org/fhir/Profile/questionnaire-extensions#flyover">
        <valueString value="An Extension"/>
      </extension>
      <linkId value="Provenance.extension"/>
      <text value="Extension"/>
      <required value="false"/>
      <repeats value="true"/>
    </group>
    <group>
      <extension url="http://hl7.org/fhir/Profile/questionnaire-extensions#flyover">
        <valueString value="May be used to represent additional information that is not part of the basic definition of the resource, and that modifies the understanding of the element that contains it. Usually modifier elements provide negation or qualification. In order to make the use of extensions safe and manageable, there is a strict set of governance applied to the definition and use of extensions. Though any implementer is allowed to define an extension, there is a set of requirements that SHALL be met as part of the definition of the extension. Applications processing a resource are required to check for modifier extensions. There can be no stigma associated with the use of extensions by any application, project, or standard - regardless of the institution or jurisdiction that uses or defines the extensions.  The use of extensions is what allows the FHIR specification to retain a core level of simplicity for everyone."/>
      </extension>
      <linkId value="Provenance.modifierExtension"/>
      <text value="Extensions that cannot be ignored"/>
      <required value="false"/>
      <repeats value="true"/>
    </group>
    <group>
      <extension url="http://hl7.org/fhir/Profile/questionnaire-extensions#flyover">
        <valueString value="The Reference(s) that were generated or updated by  the activity described in this resource. A provenance can point to more than one target if multiple resources were created/updated by the same activity. Target references are usually version specific, but may not be, if a version has not been assigned or if the provenance information is part of the set of resources being maintained (i.e. a document). When using the RESTful API, the identity of the resource may not be known (especially not the version specific one); the client may either submit the resource first, and then the provenance, or it may submit both using a single transaction. See the notes on transaction for further discussion."/>
      </extension>
      <extension url="http://www.healthintersections.com.au/fhir/Profile/metadata#type">
        <valueString value="Reference"/>
      </extension>
      <linkId value="Provenance.target"/>
      <required value="true"/>
      <repeats value="true"/>
      <question>
        <extension url="http://www.healthintersections.com.au/fhir/Profile/metadata#reference">
          <valueString value="/Resource?subject=$subj&amp;patient=$subj&amp;encounter=$encounter"/>
        </extension>
        <linkId value="Provenance.target.value"/>
        <text value="Target Reference(s) (usually version specific)"/>
        <type value="reference"/>
        <required value="false"/>
        <repeats value="false"/>
      </question>
    </group>
    <group>
      <extension url="http://hl7.org/fhir/Profile/questionnaire-extensions#flyover">
        <valueString value="The period during which the activity occurred. The period can be a little arbitrary; where possible, the time should correspond to human assessment of the activity time."/>
      </extension>
      <extension url="http://www.healthintersections.com.au/fhir/Profile/metadata#type">
        <valueString value="Period"/>
      </extension>
      <linkId value="Provenance.period"/>
      <text value="When the activity occurred"/>
      <required value="false"/>
      <repeats value="true"/>
      <question>
        <linkId value="Provenance.period.low"/>
        <text value="start:"/>
        <type value="dateTime"/>
        <required value="false"/>
        <repeats value="false"/>
      </question>
      <question>
        <linkId value="Provenance.period.end"/>
        <text value="end:"/>
        <type value="dateTime"/>
        <required value="false"/>
        <repeats value="false"/>
      </question>
    </group>
    <group>
      <extension url="http://hl7.org/fhir/Profile/questionnaire-extensions#flyover">
        <valueString value="The instant of time at which the activity was recorded. This can be a little different from the time stamp on the resource if there is a delay between recording the event and updating the provenance and target resource."/>
      </extension>
      <extension url="http://www.healthintersections.com.au/fhir/Profile/metadata#type">
        <valueString value="instant"/>
      </extension>
      <linkId value="Provenance.recorded"/>
      <required value="true"/>
      <repeats value="true"/>
      <question>
        <linkId value="Provenance.recorded.value"/>
        <text value="When the activity was recorded / updated"/>
        <type value="instant"/>
        <required value="false"/>
        <repeats value="false"/>
      </question>
    </group>
    <group>
      <extension url="http://hl7.org/fhir/Profile/questionnaire-extensions#flyover">
        <valueString value="The reason that the activity was taking place."/>
      </extension>
      <extension url="http://www.healthintersections.com.au/fhir/Profile/metadata#type">
        <valueString value="CodeableConcept"/>
      </extension>
      <linkId value="Provenance.reason"/>
      <text value="Reason the activity is occurring"/>
      <required value="false"/>
      <repeats value="true"/>
      <question>
        <linkId value="Provenance.reason.coding"/>
        <text value="code:"/>
        <type value="open-choice"/>
        <required value="false"/>
        <repeats value="false"/>
      </question>
      <question>
        <linkId value="Provenance.reason.text"/>
        <text value="text:"/>
        <type value="string"/>
        <required value="false"/>
        <repeats value="false"/>
      </question>
    </group>
    <group>
      <extension url="http://hl7.org/fhir/Profile/questionnaire-extensions#flyover">
        <valueString value="Where the activity occurred, if relevant."/>
      </extension>
      <extension url="http://www.healthintersections.com.au/fhir/Profile/metadata#type">
        <valueString value="Reference"/>
      </extension>
      <linkId value="Provenance.location"/>
      <required value="false"/>
      <repeats value="true"/>
      <question>
        <extension url="http://www.healthintersections.com.au/fhir/Profile/metadata#reference">
          <valueString value="/Location?subject=$subj&amp;patient=$subj&amp;encounter=$encounter"/>
        </extension>
        <linkId value="Provenance.location.value"/>
        <text value="Where the activity occurred, if relevant"/>
        <type value="reference"/>
        <required value="false"/>
        <repeats value="false"/>
      </question>
    </group>
    <group>
      <extension url="http://hl7.org/fhir/Profile/questionnaire-extensions#flyover">
        <valueString value="Policy or plan the activity was defined by. Typically, a single activity may have multiple applicable policy documents, such as patient consent, guarantor funding, etc. For example: Where an OAuth token authorizes, the unique identifier from the OAuth token is placed into the policy element Where a policy engine (e.g. XACML) holds policy logic, the unique policy identifier is placed into the policy element."/>
      </extension>
      <extension url="http://www.healthintersections.com.au/fhir/Profile/metadata#type">
        <valueString value="uri"/>
      </extension>
      <linkId value="Provenance.policy"/>
      <required value="false"/>
      <repeats value="true"/>
      <question>
        <linkId value="Provenance.policy.value"/>
        <text value="Policy or plan the activity was defined by"/>
        <type value="string"/>
        <required value="false"/>
        <repeats value="false"/>
      </question>
    </group>
    <group>
      <extension url="http://hl7.org/fhir/Profile/questionnaire-extensions#flyover">
        <valueString value="An agent takes a role in an activity such that the agent can be assigned some degree of responsibility for the activity taking place. An agent can be a person, an organization, software, or other entities that may be ascribed responsibility."/>
      </extension>
      <linkId value="Provenance.agent"/>
      <title value="Agents involved in creating resource"/>
      <text value="Several agents may be associated (i.e. has some responsibility for an activity) with an activity and vice-versa."/>
      <required value="false"/>
      <repeats value="true"/>
      <group>
        <extension url="http://hl7.org/fhir/Profile/questionnaire-extensions#flyover">
          <valueString value="unique id for the element within a resource (for internal references)."/>
        </extension>
        <extension url="http://www.healthintersections.com.au/fhir/Profile/metadata#type">
          <valueString value="string"/>
        </extension>
        <linkId value="Provenance.agent.id"/>
        <required value="false"/>
        <repeats value="true"/>
        <question>
          <linkId value="Provenance.agent.id.value"/>
          <text value="xml:id (or equivalent in JSON)"/>
          <type value="string"/>
          <required value="false"/>
          <repeats value="false"/>
        </question>
      </group>
      <group>
        <extension url="http://hl7.org/fhir/Profile/questionnaire-extensions#flyover">
          <valueString value="An Extension"/>
        </extension>
        <linkId value="Provenance.agent.extension"/>
        <text value="Extension"/>
        <required value="false"/>
        <repeats value="true"/>
      </group>
      <group>
        <extension url="http://hl7.org/fhir/Profile/questionnaire-extensions#flyover">
          <valueString value="May be used to represent additional information that is not part of the basic definition of the element, and that modifies the understanding of the element that contains it. Usually modifier elements provide negation or qualification. In order to make the use of extensions safe and manageable, there is a strict set of governance applied to the definition and use of extensions. Though any implementer is allowed to define an extension, there is a set of requirements that SHALL be met as part of the definition of the extension. Applications processing a resource are required to check for modifier extensions. There can be no stigma associated with the use of extensions by any application, project, or standard - regardless of the institution or jurisdiction that uses or defines the extensions.  The use of extensions is what allows the FHIR specification to retain a core level of simplicity for everyone."/>
        </extension>
        <linkId value="Provenance.agent.modifierExtension"/>
        <text value="Extensions that cannot be ignored"/>
        <required value="false"/>
        <repeats value="true"/>
      </group>
      <group>
        <extension url="http://hl7.org/fhir/Profile/questionnaire-extensions#flyover">
          <valueString value="The function of the agent with respect to the activity. e.g. author | performer | enterer | attester: +."/>
        </extension>
        <extension url="http://www.healthintersections.com.au/fhir/Profile/metadata#type">
          <valueString value="Coding"/>
        </extension>
        <linkId value="Provenance.agent.role"/>
        <required value="true"/>
        <repeats value="true"/>
        <question>
          <linkId value="Provenance.agent.role.value"/>
          <text value="Agents Role"/>
          <type value="open-choice"/>
          <required value="false"/>
          <repeats value="false"/>
        </question>
      </group>
      <group>
        <extension url="http://hl7.org/fhir/Profile/questionnaire-extensions#flyover">
          <valueString value="The individual, device or organization that participated in the event."/>
        </extension>
        <linkId value="Provenance.agent.actor"/>
        <text value="Individual, device or organization playing role"/>
        <required value="false"/>
        <repeats value="true"/>
        <question>
          <linkId value="Provenance.agent.actor._type"/>
          <text value="type"/>
          <type value="choice"/>
          <required value="false"/>
          <repeats value="false"/>
          <options>
            <reference value="#vs2"/>
          </options>
          <group>
            <extension url="http://www.healthintersections.com.au/fhir/Profile/metadata#type">
              <valueString value="Reference"/>
            </extension>
            <linkId value="Provenance.agent.actor._Practitioner"/>
            <question>
              <extension url="http://www.healthintersections.com.au/fhir/Profile/metadata#reference">
                <valueString value="/Practitioner?subject=$subj&amp;patient=$subj&amp;encounter=$encounter"/>
              </extension>
              <linkId value="Provenance.agent.actor._Practitioner.value"/>
              <text value="Practitioner"/>
              <type value="reference"/>
              <required value="false"/>
              <repeats value="false"/>
            </question>
          </group>
          <group>
            <extension url="http://www.healthintersections.com.au/fhir/Profile/metadata#type">
              <valueString value="Reference"/>
            </extension>
            <linkId value="Provenance.agent.actor._RelatedPerson"/>
            <question>
              <extension url="http://www.healthintersections.com.au/fhir/Profile/metadata#reference">
                <valueString value="/RelatedPerson?subject=$subj&amp;patient=$subj&amp;encounter=$encounter"/>
              </extension>
              <linkId value="Provenance.agent.actor._RelatedPerson.value"/>
              <text value="RelatedPerson"/>
              <type value="reference"/>
              <required value="false"/>
              <repeats value="false"/>
            </question>
          </group>
          <group>
            <extension url="http://www.healthintersections.com.au/fhir/Profile/metadata#type">
              <valueString value="Reference"/>
            </extension>
            <linkId value="Provenance.agent.actor._Patient"/>
            <question>
              <extension url="http://www.healthintersections.com.au/fhir/Profile/metadata#reference">
                <valueString value="/Patient?subject=$subj&amp;patient=$subj&amp;encounter=$encounter"/>
              </extension>
              <linkId value="Provenance.agent.actor._Patient.value"/>
              <text value="Patient"/>
              <type value="reference"/>
              <required value="false"/>
              <repeats value="false"/>
            </question>
          </group>
          <group>
            <extension url="http://www.healthintersections.com.au/fhir/Profile/metadata#type">
              <valueString value="Reference"/>
            </extension>
            <linkId value="Provenance.agent.actor._Device"/>
            <question>
              <extension url="http://www.healthintersections.com.au/fhir/Profile/metadata#reference">
                <valueString value="/Device?subject=$subj&amp;patient=$subj&amp;encounter=$encounter"/>
              </extension>
              <linkId value="Provenance.agent.actor._Device.value"/>
              <text value="Device"/>
              <type value="reference"/>
              <required value="false"/>
              <repeats value="false"/>
            </question>
          </group>
        </question>
      </group>
      <group>
        <extension url="http://hl7.org/fhir/Profile/questionnaire-extensions#flyover">
          <valueString value="The identify of the agent as known by the authorization system."/>
        </extension>
        <extension url="http://www.healthintersections.com.au/fhir/Profile/metadata#type">
          <valueString value="Identifier"/>
        </extension>
        <linkId value="Provenance.agent.userId"/>
        <text value="Authorization-system identifier for the agent"/>
        <required value="false"/>
        <repeats value="true"/>
        <question>
          <linkId value="Provenance.agent.userId.label"/>
          <text value="label:"/>
          <type value="string"/>
          <required value="false"/>
          <repeats value="false"/>
        </question>
        <question>
          <linkId value="Provenance.agent.userId.system"/>
          <text value="system:"/>
          <type value="string"/>
          <required value="false"/>
          <repeats value="false"/>
        </question>
        <question>
          <linkId value="Provenance.agent.userId.value"/>
          <text value="value:"/>
          <type value="string"/>
          <required value="false"/>
          <repeats value="false"/>
        </question>
      </group>
    </group>
    <group>
      <extension url="http://hl7.org/fhir/Profile/questionnaire-extensions#flyover">
        <valueString value="An entity used in this activity."/>
      </extension>
      <linkId value="Provenance.entity"/>
      <title value="An entity used in this activity"/>
      <text value="Multiple userIds may be associated with the same Practitioner or other individual across various appearances, each with distinct privileges."/>
      <required value="false"/>
      <repeats value="true"/>
      <group>
        <extension url="http://hl7.org/fhir/Profile/questionnaire-extensions#flyover">
          <valueString value="unique id for the element within a resource (for internal references)."/>
        </extension>
        <extension url="http://www.healthintersections.com.au/fhir/Profile/metadata#type">
          <valueString value="string"/>
        </extension>
        <linkId value="Provenance.entity.id"/>
        <required value="false"/>
        <repeats value="true"/>
        <question>
          <linkId value="Provenance.entity.id.value"/>
          <text value="xml:id (or equivalent in JSON)"/>
          <type value="string"/>
          <required value="false"/>
          <repeats value="false"/>
        </question>
      </group>
      <group>
        <extension url="http://hl7.org/fhir/Profile/questionnaire-extensions#flyover">
          <valueString value="May be used to represent additional information that is not part of the basic definition of the element. In order to make the use of extensions safe and manageable, there is a strict set of governance  applied to the definition and use of extensions. Though any implementer is allowed to define an extension, there is a set of requirements that SHALL be met as part of the definition of the extension. There can be no stigma associated with the use of extensions by any application, project, or standard - regardless of the institution or jurisdiction that uses or defines the extensions.  The use of extensions is what allows the FHIR specification to retain a core level of simplicity for everyone."/>
        </extension>
        <linkId value="Provenance.entity.extension"/>
        <text value="Additional Content defined by implementations"/>
        <required value="false"/>
        <repeats value="true"/>
      </group>
      <group>
        <extension url="http://hl7.org/fhir/Profile/questionnaire-extensions#flyover">
          <valueString value="May be used to represent additional information that is not part of the basic definition of the element, and that modifies the understanding of the element that contains it. Usually modifier elements provide negation or qualification. In order to make the use of extensions safe and manageable, there is a strict set of governance applied to the definition and use of extensions. Though any implementer is allowed to define an extension, there is a set of requirements that SHALL be met as part of the definition of the extension. Applications processing a resource are required to check for modifier extensions. There can be no stigma associated with the use of extensions by any application, project, or standard - regardless of the institution or jurisdiction that uses or defines the extensions.  The use of extensions is what allows the FHIR specification to retain a core level of simplicity for everyone."/>
        </extension>
        <linkId value="Provenance.entity.modifierExtension"/>
        <text value="Extensions that cannot be ignored"/>
        <required value="false"/>
        <repeats value="true"/>
      </group>
      <group>
        <extension url="http://hl7.org/fhir/Profile/questionnaire-extensions#flyover">
          <valueString value="How the entity was used during the activity."/>
        </extension>
        <extension url="http://www.healthintersections.com.au/fhir/Profile/metadata#type">
          <valueString value="code"/>
        </extension>
        <linkId value="Provenance.entity.role"/>
        <required value="true"/>
        <repeats value="true"/>
        <question>
          <linkId value="Provenance.entity.role.value"/>
          <text value="role"/>
          <type value="choice"/>
          <required value="false"/>
          <repeats value="false"/>
        </question>
      </group>
      <group>
        <extension url="http://hl7.org/fhir/Profile/questionnaire-extensions#flyover">
          <valueString value="The type of the entity. If the entity is a resource, then this is a resource type."/>
        </extension>
        <extension url="http://www.healthintersections.com.au/fhir/Profile/metadata#type">
          <valueString value="Coding"/>
        </extension>
        <linkId value="Provenance.entity.type"/>
        <required value="true"/>
        <repeats value="true"/>
        <question>
          <linkId value="Provenance.entity.type.value"/>
          <text value="Entity Type"/>
          <type value="open-choice"/>
          <required value="false"/>
          <repeats value="false"/>
        </question>
      </group>
      <group>
        <extension url="http://hl7.org/fhir/Profile/questionnaire-extensions#flyover">
          <valueString value="Identity of the  Entity used. May be a logical or physical uri and maybe absolute or relative. identity may be a reference to a resource or to something else, depending on the type."/>
        </extension>
        <extension url="http://www.healthintersections.com.au/fhir/Profile/metadata#type">
          <valueString value="uri"/>
        </extension>
        <linkId value="Provenance.entity.reference"/>
        <required value="true"/>
        <repeats value="true"/>
        <question>
          <linkId value="Provenance.entity.reference.value"/>
          <text value="Identity of entity"/>
          <type value="string"/>
          <required value="false"/>
          <repeats value="false"/>
        </question>
      </group>
      <group>
        <extension url="http://hl7.org/fhir/Profile/questionnaire-extensions#flyover">
          <valueString value="Human-readable description of the entity."/>
        </extension>
        <extension url="http://www.healthintersections.com.au/fhir/Profile/metadata#type">
          <valueString value="string"/>
        </extension>
        <linkId value="Provenance.entity.display"/>
        <required value="false"/>
        <repeats value="true"/>
        <question>
          <linkId value="Provenance.entity.display.value"/>
          <text value="Human description of entity"/>
          <type value="string"/>
          <required value="false"/>
          <repeats value="false"/>
        </question>
      </group>
      <group>
        <extension url="http://hl7.org/fhir/Profile/questionnaire-extensions#flyover">
          <valueString value="The entity is attributed to an agent to express the agent&#39;s responsibility for that entity, possibly along with other agents. This description can be understood as shorthand for saying that the agent was responsible for the activity which generated the entity."/>
        </extension>
        <linkId value="Provenance.entity.agent"/>
        <title value="Entity is attributed to this agent"/>
        <required value="false"/>
        <repeats value="false"/>
      </group>
    </group>
    <group>
      <extension url="http://hl7.org/fhir/Profile/questionnaire-extensions#flyover">
        <valueString value="A digital signature on the target Reference(s). The signer should match a Provenance.agent. The purpose of the signature is indicated."/>
      </extension>
      <linkId value="Provenance.signature"/>
      <text value="Signature on target"/>
      <required value="false"/>
      <repeats value="true"/>
    </group>
  </group>
</Questionnaire>